﻿using Newtonsoft.Json;
using PSI_3_REST_ISS.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PSI_3_REST_ISS
{
    class Program
    {
        private static string ISS_LOCATION_API_URL = "http://api.open-notify.org/iss-now.json";
        private static string ISS_LOCATION_RESPONSE_OK = "success";

        private static string SUN_DATA_API_URL = "https://api.sunrise-sunset.org/json";
        private static string SUN_DATA_RESPONSE_OK = "ok";
        static void Main(string[] args)
        {
            Console.WriteLine("ISS Location Fetcher");
            FetchISSData();
        }

        static void FetchISSData()
        {
            //ISS DAta fetch
            Console.WriteLine("Fetching ISS data ...");
            string jsonLocation = "";
            using (WebClient wc = new WebClient())
            {
                jsonLocation = wc.DownloadString(ISS_LOCATION_API_URL);
            }
            ISSData issLocation = JsonConvert.DeserializeObject<ISSData>(jsonLocation);

            if(issLocation.Message.ToLower() != ISS_LOCATION_RESPONSE_OK)
            {
                Console.WriteLine("ISS data fetch error ...");
                return;
            }

            //Sunrise/Sunset data
            Console.WriteLine("Fetching sun data ...");
            string jsonSuninfo = "";
            using (WebClient wc = new WebClient())
            {
                jsonSuninfo = wc.DownloadString($"{SUN_DATA_API_URL}?lat={issLocation.IssPosition.Latitude}&lng={issLocation.IssPosition.Longitude}&formatted=0");
            }
            SunData sunInfo = JsonConvert.DeserializeObject<SunData>(jsonSuninfo);

            if (sunInfo.Status.ToLower() != SUN_DATA_RESPONSE_OK)
            {
                Console.WriteLine("Sun data fetch error ...");
                return;
            }

            DateTime issTime = UnixTimeStampToDateTime(issLocation.Timestamp);
            string litResult = (issTime >= sunInfo.Results.Sunrise.ToUniversalTime() && issTime <= sunInfo.Results.Sunset.ToUniversalTime()) ? "lit" : "unlit";
            string monitoringResult = (issTime >= sunInfo.Results.Sunset.AddHours(1.5).ToUniversalTime() && issTime <= sunInfo.Results.Sunrise.AddHours(-1.5).ToUniversalTime()) ? "ideal" : "not ideal";

            Console.WriteLine("FETCH DONE");
            Console.WriteLine("");

            Console.WriteLine("Printing results:");
            Console.WriteLine($"Latitude: {issLocation.IssPosition.Latitude}");
            Console.WriteLine($"Longitude: {issLocation.IssPosition.Longitude}");
            Console.WriteLine("");

            Console.WriteLine($"Earth under the station is {litResult}");
            Console.WriteLine($"Station monitoring conditions are {monitoringResult}");

            Console.WriteLine("");
            Console.WriteLine("Program ended");

        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
