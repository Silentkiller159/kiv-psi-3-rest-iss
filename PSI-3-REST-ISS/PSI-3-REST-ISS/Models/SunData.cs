﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSI_3_REST_ISS.Models
{
    public class SunData
    {
        public string Status { get; set; }
        public SunResponse Results { get; set; }
    }

    public class SunResponse
    {
        public DateTime Sunrise { get; set; }

        public DateTime Sunset { get; set; }

        [JsonProperty(PropertyName = "solar_noon")]
        public DateTime SolarNoon { get; set; }

        [JsonProperty(PropertyName = "day_length")]
        public double DayLength { get; set; }

        [JsonProperty(PropertyName = "civil_twilight_begin")]
        public DateTime CivilTwilightBegin { get; set; }

        [JsonProperty(PropertyName = "civil_twilight_end")]
        public DateTime CivilTwilightEnd { get; set; }

        [JsonProperty(PropertyName = "nautical_twilight_begin")]
        public DateTime NauticalTwilightBegin { get; set; }

        [JsonProperty(PropertyName = "nautical_twilight_end")]
        public DateTime NauticalTwilightEnd { get; set; }

        [JsonProperty(PropertyName = "astronomical_twilight_begin")]
        public DateTime AstronomicalTwilightBegin { get; set; }

        [JsonProperty(PropertyName = "astronomical_twilight_end")]
        public DateTime AstronomicalTwilightEnd { get; set; }
    }
}
