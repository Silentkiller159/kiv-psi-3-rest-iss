﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSI_3_REST_ISS.Models
{
    public class ISSData
    {
        public string Message { get; set; }
        public double Timestamp { get; set; }

        [JsonProperty(PropertyName = "iss_position")]
        public ISSPosition IssPosition { get; set; }
    }

    public class ISSPosition
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
