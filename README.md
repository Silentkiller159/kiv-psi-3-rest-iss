# KIV-PSI-3-ISS_REST

## Description of logic
- Fetching ISS Data into prepared object (ISSData)
- Fetching Sun Data into prepared object (SunData)
- Executing evaluation logic with obtained data
- Printing results

## How to use
- Execute "dotnet run" command in project folder (./PSI-3-REST-ISS) where .csproj is located

## Dependencies
- .NET 3.1 Runtime (or higher)
